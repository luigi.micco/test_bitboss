<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Application;
use Faker\Generator as Faker;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $user_ids = User::where('is_admin', 0)->pluck('id')->all();

        foreach ($user_ids as $id) {

            // per alcuni utenti noncrea l'applicazione 
            if ($id % 4 == 0) continue;

            $application = new Application();
            $application->user_id = $id;
            $application->first_name = $faker->firstName();
            $application->last_name = $faker->lastName();
            $application->email = $faker->email;
            $application->phone = $faker->randomNumber(3, true) . $faker->randomNumber($faker->numberBetween(6, 8), true);
            $application->notes = $faker->text(500);
            $application->status = $faker->numberBetween(0, 2);

            $application->save();
        }
    }
}
