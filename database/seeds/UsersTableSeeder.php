<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = Hash::make('admin');
        $user->is_admin = true;
        $user->save();

        for ($i = 1; $i < 40; $i++) {
            $username = 'utente' . str_pad($i, 2, '0', STR_PAD_LEFT);
            User::create([
                'name' => $username,
                'email' => $username . '@utenti.com',
                'password' => Hash::make($username),
            ]);
        }
    }
}
