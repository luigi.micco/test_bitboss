<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Application;
use Notification;
use App\Notifications\NewApplication;
use App\Notifications\SlackNotifiable;
use App\Models\User;
use Auth;

class PagesController extends Controller
{

    // 'first_name', 'last_name', 'email', 'phone', 'notes'

    protected $rules =
    [
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255',
        'email' => 'required|email|max:255|unique:applications',
        'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:20',
        'notes' => 'nullable',
    ];

    protected $messages = [
        'first_name.required' => 'E\' necessario inserire il nome',
        'first_name.max' => 'Il nome non può essere più lungo di 255 caratteri',

        'last_name.required' => 'E\' necessario inserire il cognome',
        'last_name.max' => 'Il cognome non può essere più lungo di 255 caratteri',

        'email.required' => 'E\' necessario inserire un indirizzo mail',
        'email.max' => 'L\'indirizzo mail non può essere più lungo di 255 caratteri',
        'email.email' => 'L\'indirizzo mail deve essere valido',
        'email.unique' => 'Indirizzo mail già presente in archivo',

        'phone.required' => 'E\' necessario inserire un numero telefonico',
        'phone.max' => 'Il numero telefonico non può essere più lungo di 20 cifre',
        'phone.min' => 'Il numero telefonico non può essere più corto di 8 cifre',
        'phone.regex' => 'Caratteri non validi nel numero telefonico',
    ];

    public function homepage()
    {
        return view('home');
    }

    public function apply()
    {

        // verifica se esiste già un record per lo stesso utente    
        $user_id = Auth::user()->id;
        if (Application::where('user_id', $user_id)->exists()) {
            // se esiste rinvia alla pagina dello status
            return  redirect()->route('status');
        } else {
            return view('apply');
        }
    }

    public function postApply(Request $request)
    {

        $request->validate($this->rules, $this->messages);
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $newApplication = new Application();
        $newApplication->fill($data);
        $newApplication->save();

        // send notification

        // via email to admins
        $admins = User::where('is_admin', true)->get();
        Notification::send($admins, new NewApplication($newApplication));

        // via slack
        // update .ENV with a key
        // SLACK_GENERAL_WEBHOOK_URL= _slack_webhook_url_
        (new SlackNotifiable())->notify(new NewApplication());

        return redirect()->route('home');
    }

    public function status()
    {
        $user_id = Auth::user()->id;
        $application = Application::where('user_id', $user_id)->first();

        return view('situation', compact('application'));
    }
}
