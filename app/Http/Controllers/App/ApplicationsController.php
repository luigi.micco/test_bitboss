<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;
use Notification;
use App\Notifications\ApplicationUpdate;
use App\Models\User;

class ApplicationsController extends Controller
{

    public function __construct()
    {
        // Accessible only to admins
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->expectsJson())
            return Application::paginate(20);

        return view('applications.index');
    }

    public function accept(Request $request)
    {
        $data = $request->all();
        $application = Application::findOrFail($data['id']);
        $application->status = 2;
        $application->save();

        // send notification to user
        $user = User::where('id', $application->user_id)->get();
        Notification::send($user, new ApplicationUpdate($application));
    }

    public function refuse(Request $request)
    {
        $data = $request->all();
        $application = Application::findOrFail($data['id']);
        $application->status = 1;
        $application->save();

        // send notification to user
        $user = User::where('id', $application->user_id)->get();
        Notification::send($user, new ApplicationUpdate($application));
    }
}
