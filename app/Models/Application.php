<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $guarded = [];

    // elenco dei campi che arrivano dalla form
    protected $fillable = ['user_id', 'first_name', 'last_name', 'email', 'phone', 'notes'];
}
