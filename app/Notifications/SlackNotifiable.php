<?php

namespace App\Notifications;

use Illuminate\Notifications\Notifiable;

class SlackNotifiable
{
  use Notifiable;

  private $email = '';

  public function routeNotificationForSlack()
  {
    return config('services.slack.general');
  }
}
