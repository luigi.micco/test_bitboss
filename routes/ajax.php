<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| AJAX Routes
|--------------------------------------------------------------------------
*/

/**
 * Routes per la gestione delle candidature
 */
Route::prefix('applications')
  ->name('applications.')
  ->group(function () {
    Route::post('/accept_application', 'App\\ApplicationsController@accept')->name('accept_application');
    Route::post('/refuse_application', 'App\\ApplicationsController@refuse')->name('refuse_application');
  });
