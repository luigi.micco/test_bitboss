@extends('layouts.app')

@section('content')

<section class="container mt-4">
    <div class="row">

        <div class="col-12 ">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h2>Applications</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">  
                    <applications inline-template>
                        <div>
                            <table class="w-100">
                                <thead>
                                    <th class="w-30">First name</th>
                                    <th class="w-30">Last name</th>
                                    <th class="text-center w-10">State</th>
                                    <th class="text-center">Actions</th>
                                </thead>

                                <tr v-for="(application, index) in items" :key="application.id">
                                    <td>@{{ application.first_name }}</td>
                                    <td>@{{ application.last_name }}</td>
                                    <td class="text-center">@{{ (application.status == 0) ? "-" : ((application.status == 2) ? "Accepted" : "Refused") }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-success" :class="(application.status == 2) ? 'disabled':''" href="javascript:;" @click="accept(application.id)">{{__("Approve")}}</a> 
                                        <a class="btn btn-warning" :class="(application.status == 1) ? 'disabled':''"  href="javascript:;" @click="refuse(application.id)">{{__("Refuse")}}</a>
                                    </td>
                                </tr>
                            </table>

                            <paginator :dataSet="dataSet" @changed="fetch"></paginator>
                        </div>
                    </applications>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection