@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Candidatura</h1>
        </div>
    </section>

    <section class="jumbotron text-center">
        <div class="container">
            @if ($application)
                <h2>{{__("Your application is ") }} 
                @if ($application->status == 2)
                    <b>Accepted</b>
                @else
                    <b>Refused</b>
                @endif
                at {{date_format($application->updated_at,"d/m/Y H:i")}}
                </h2>
            @else
                <h2 >Apply now to become a BitBoss developer</h2>
                <p>
                    <a href="{{route('apply')}}" class="btn btn-primary my-2">Apply</a>
                </p>
            @endif
        </div>
    </section>
@stop
