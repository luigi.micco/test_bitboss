@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <img src="https://www.bitboss.it/assets/bitboss_logo.svg"/>
        <div class="container">
            <h1>Candidati</h1>
            <p class="lead">Presenta la tua candidatura</p>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="container">
                {!! Form::open(['route' => ['postApply'], 'method' => 'post'])!!}
                <div class="form-group">
                    {!! Form::label('first_name', 'Nome') !!}
                    {!! Form::text('first_name', null, ['class' => 'form-control ' .($errors->has('first_name') ? 'is-invalid': ''), 'placeholder'=>'Nome']) !!}
                    @if ($errors->has('first_name'))
                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Cognome') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control ' .($errors->has('last_name') ? 'is-invalid': ''), 'placeholder'=>'Cognome']) !!}
                    @if ($errors->has('last_name'))
                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                    @endif                    
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', null, ['class' => 'form-control ' .($errors->has('email') ? 'is-invalid': ''), 'placeholder'=>'Email']) !!}
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif  
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Telefono') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control ' .($errors->has('phone') ? 'is-invalid': ''), 'placeholder'=>'Telefono']) !!}
                    @if ($errors->has('phone'))
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                    @endif 
                </div>

                <div class="form-group">
                    {!! Form::label('notes', 'Note') !!}
                    {!! Form::textarea('notes', null, ['rows'=>3, 'class' => 'form-control', 'placeholder'=>'Note']) !!}
                </div>

                <div>

                    <button type="submit" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-paper-plane"
                                                                                              aria-hidden="true"></i>
                        {{__("Invia candidatura")}}
                    </button>
                </div>

                {!! Form::close() !!}
            </div>
    </section>

@stop
